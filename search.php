<?php
include('config/config.php');
global $map, $fork, $db, $noSearch, $noGyms, $noPokestops, $noRaids;
if($noSearch === true || ($noGyms && $noRaids && $noPokestops)){
    http_response_code(401);
    die();
}
$term = !empty($_POST['term']) ? $_POST['term'] : '';
$action = !empty($_POST['action']) ? $_POST['action'] : '';
$dbname = '';
if($action === "pokestops") {
    $dbname = "pokestops";
}
elseif($action === "forts") {
    $dbname = "gym a inner join gymdetails b on a.gym_id = b.gym_id";
}
if($dbname !== '') {
    if($db->info()['driver'] === 'mysql' || 'mariadb') {
        if (!strpos($term, ';')){
        $data = $db->query("SELECT a.gym_id, b.name, a.latitude,  a.longitude, b.url FROM " . $dbname . " WHERE LOWER(b.name) LIKE '%" . $term . "%' LIMIT 10;")->fetchALL();}
    }
    else {
        $data = $db->select($action, ['id', 'external_id', 'name', 'lat', 'lon', 'url'], ['name[~]' => $term, 'LIMIT' => 10]);
    }
    // set content type
    header('Content-Type: application/json');

    $jaysson = json_encode($data);
    echo $jaysson;
}
