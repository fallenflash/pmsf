<?php

class datas {

    public function __construct($data) {
        
        $this->post = $this->populatedata($data);
        $this->boss = $this->boss();
        return $this;
    }

    private function populateData($data) {
        global $post;
        $recieved = $data;

        $post = new stdclass;
        $post->montime = $recieved['montime'];
        $post->eggtime = $recieved['eggtime'];
        $post->gym_id = $recieved['gymid'];
        $post->pokemon_id = $recieved['pokemonid'];
        return $post;

    }
    private function boss(){
        $boss = json_decode(file_get_contents("static/dist/data/raid-boss.min.json"), true);
        return $boss;
    }


}


class timers{


    public function __construct() {
        global $post;
        if (strpos($post->pokemon_id,'egg_') !== false) {
        $data =  $this->egg();
        } else {
        $data = $this->mon();
        }
        return $data;
    }

    public function times() {
        global $post;
        
        $t = new datetime();
        $t = $t->setTimezone(new DateTimeZone("UTC"));
        $this->now = $t;

        $this->hour = new dateinterval("PT1H");
        $this->fortyfive = new dateinterval("PT45M");
        $this->montime = new dateinterval('PT'.$post->montime.'M');
        $this->eggtime = new dateinterval('PT'.$post->eggtime.'M');
        return $this;
    }
    

    Public function egg() {

        $format = 'Y-m-d H:i:s';
        $time = $this->times();
        $this->now = $time->now;

        $hatch = clone $time;
        $hatch = $hatch->now->add($time->eggtime);
        $this->hatch = $hatch->format($format);

        $spawn = clone $hatch;
        $spawn = $spawn->sub($time->hour);
        $this->spawn = $spawn->format($format);

        $end = clone $hatch;
        $end = $end->add($time->fortyfive);
        $this->end = $end->format($format);

    }
    Public function mon() {
        
        $format = 'Y-m-d H:i:s';
        $time = $this->times();

        $end = clone $time;
        $end = $end->now->add($time->montime);
        $this->end = $end->format($format);

        $hatch = clone $end;
        $hatch = $hatch->sub($time->fortyfive);
        $this->hatch = $hatch->format($format);

        $spawn = clone $hatch;
        $spawn = $spawn->sub($time->hour);
        $this->spawn = $spawn->format($format);

    }
}
    class upsert {
    
        public function __construct(){
        
            $this->update();
        }
        private function prepair_cols(){
            global $post;
            global $timers;
            global $datas;
            if (strpos($post->pokemon_id,'egg_') !== false) {
            $cols = [
                'gym_id' => $post->gym_id,
                'level' => $level = (int)substr($post->pokemon_id,4,1),
                'spawn' => $timers->spawn,
                'start' => $timers->hatch,
                'end' => $timers->end,
                'pokemon_id' => NULL,
                'cp' => NULL,
                'move_1' => NULL, // struggle
                'move_2' => NULL,
                'last_scanned' => gmdate('Y-m-d H:i:s')
            ];
            return $cols;
        } elseif (array_key_exists($post->pokemon_id, $datas->boss)){
            $cols = [
                'gym_id' => $post->gym_id,
                'level' => $datas->boss[$post->pokemon_id]['level'],
                'spawn' => $timers->spawn,
                'start' => $timers->hatch,
                'end' => $timers->end,
                'pokemon_id' => $post->pokemon_id,
                'cp' => $datas->boss[$post->pokemon_id]['cp'],
                'move_1' => 1, // struggle
                'move_2' => 1,
                'last_scanned' => gmdate('Y-m-d H:i:s')
            ];
            return $cols;
        } elseif($cols['level'] === 0) {
            // no boss or egg matched
            http_response_code(500);
            }
        }
        private function update(){
            global $post;
            global $db;
            $cols = $this->prepair_cols();
            $db->query("DELETE FROM raid WHERE gym_id = '".$post->gym_id."'");
            $db->insert("raid", $cols);
        }
    }

    class response {
        public function __construct() {

        header('Content-Type: application/json');

        $now = new DateTime();
        $now->sub(new DateInterval('PT20S'));

        $d = array();
        $d['status'] = "ok";
        $d["timestamp"] = $now->getTimestamp();

        $js = json_encode($d);
        echo $js;
    }
}


?>